﻿using System;
using Manager;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Test
{
    public class TestButton : MonoBehaviour
    {
        public void Back()
        {
            SceneManager.LoadScene("Game");
            SoundManager.Instance.StopPlaySound(SoundManager.Sound.GameOver);
            SoundManager.Instance.Play(SoundManager.Sound.BGM);
        }

        public void QuitGame()
        {
            Application.Quit();
            EditorApplication.isPlaying = false;
        }
    }
}
