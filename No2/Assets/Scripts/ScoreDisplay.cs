﻿using System;
using System.Collections;
using System.Collections.Generic;
using Manager;
using UnityEngine;
using UnityEngine.UI;

public class ScoreDisplay : MonoBehaviour
{
    Text scoreText;
    ScoreManager scoreManager;

    void Start()
    {
        scoreText = GetComponent<Text>();
        scoreManager = FindObjectOfType<ScoreManager>();
    }

    private void Update()
    {
        scoreText.text = ScoreManager.Instance.GetScore().ToString();
    }
}
