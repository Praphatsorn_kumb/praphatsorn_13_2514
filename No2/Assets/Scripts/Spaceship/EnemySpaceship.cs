using System;
using Manager;
using UnityEngine;

namespace Spaceship
{
    public class EnemySpaceship : Basespaceship, IDamagable
    {
        public event Action OnExploded;

        [Header("Enemy Value")]
        [SerializeField] private int scoreValue = 150;
        
        [Header("Shooting")]
        [SerializeField] private double enemyFireRate = 0.5;
        private float fireCounter = 0f;
        
        
        private void Awake()
        {
            Debug.Assert(enemyFireRate > 0, "enemyFireRate has to be more than zero");            
        }
        
        public void Init(int hp, float speed)
        {
            base.Init(hp, speed, defaultBullet);
        }
        
        public void TakeHit(int damage)
        {
            Hp -= damage;

            if (Hp > 0)
            {
                return;
            }
            
            Explode();
        }

        public void Explode()
        {
            FindObjectOfType<ScoreManager>().UpdateScore(scoreValue);
            SoundManager.Instance.Play(SoundManager.Sound.EnemyExplode);
            Debug.Assert(Hp <= 0, "HP is more than zero");
            gameObject.SetActive(false);
            Destroy(gameObject);
            OnExploded?.Invoke();
        }

        public override void Fire()
        {
            fireCounter += Time.deltaTime;
            if (fireCounter >= enemyFireRate)
            {
                SoundManager.Instance.Play(SoundManager.Sound.EnemyFire);
                //var bullet = Instantiate(defaultBullet, gunPosition.position, Quaternion.identity);
                //bullet.Init(Vector2.down);
                var bullet = PoolManager.Instance.GetPooledObject(PoolManager.PoolObjectType.EnemyBullet);
                if (bullet)
                {
                    bullet.transform.position = gunPosition.position;
                    bullet.transform.rotation = Quaternion.identity;
                    bullet.SetActive(true);
                    bullet.GetComponent<Bullet>().Init(Vector2.down);    
                }
                
                fireCounter = 0;
            }
        }
    }
}